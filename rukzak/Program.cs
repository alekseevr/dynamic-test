﻿using System;
using System.Collections.Generic;

namespace rukzak
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            var items = new Dictionary<Int32, Int32>
            {
                {8, 50}, 
                {1, 2}
            };

            const Int32 W = 10;
            var A = new Int32[W + 1];
            
            // слепое следование алгоритму (инициализация)
            A[0] = 0;

            for (var w = 1; w <= W; w++)
            {
                A[w] = GetMax(w, A, items);
            }

            Print(A);

            Console.ReadKey();
        }

        private static Int32 GetMax(Int32 w, Int32[] A, Dictionary<Int32, Int32> items)
        {
            var max = -1;
            
            foreach (var item in items)
            {
                var wi = item.Key;
                var Ci = item.Value;
                
                if (wi <= w)
                {
                    var wannabe = A[w - wi] + Ci;
                    
                    if (wannabe > max)
                    {
                        max = wannabe;
                    }
                }
            }

            return max;
        }

        private static void Print(Int32[] A)
        {
            var i = 0;
            
            foreach (var a in A)
            {
                Console.WriteLine($"{i++}: {a}");
            }
        }
    }
}