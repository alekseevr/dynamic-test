﻿using System;

namespace rukzak_unique
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            const Int32 n = 4;
            
            var weights = new Int32[n + 1];
            weights[1] = 5;
            weights[2] = 7;
            weights[3] = 4;
            weights[4] = 2;

            var costs = new Int32[n + 1];
            costs[1] = 3;
            costs[2] = 2;
            costs[3] = 10;
            costs[4] = 1;
            
            const Int32 W = 10;
            var A = new Int32[W + 1, n + 1];
            
            for (var i = 1; i <= n; i++)
            {
                for (var w = 1; w <= W; w++)
                {
                    // предмет помещается 
                    if (weights[i] <= w)
                    {
                        var wi = weights[i];
                        var ci = costs[i];
                        A[w, i] = Math.Max(A[w - wi, i - 1] + ci, A[w, i - 1]);
                    }
                    // предмет не помещается
                    else
                    {
                        A[w, i] = A[w, i - 1];
                    }
                }
            }
            
            Print(A);
            
            Console.ReadKey();
        }

        private static void Print(Int32[,] arr)
        {
            var rowLength = arr.GetLength(0);
            var colLength = arr.GetLength(1);

            for (var i = 0; i < rowLength; i++)
            {
                for (var j = 0; j < colLength; j++)
                {
                    Console.Write($"{arr[i, j]} ");
                }
                
                Console.Write(Environment.NewLine + Environment.NewLine);
            }
        }
    }
}