﻿using System;
using System.Collections.Generic;

namespace dynamic_test
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var A = new List<Int32> { 4, 5, 7, 2, 1, 5, 18, 3 };
            var D = new Int32[A.Count];
            D[0] = 1;
            var n = A.Count;

            for (var i = 1; i < n; i++)
            {
                var (index, found) = FindMaxIndex(A, D, i);
                D[i] = 1 + (found ? D[index] : 0);
            }

            // 1 2 3 1 1 2 4 2
            foreach (var d in D)
            {
                Console.WriteLine(d);
            }

            Console.ReadKey();
        }

        /// <summary>
        /// Максимум по пустому множеству - это 0 (множество пустое, если нет ни одного j, меньшего или равного i - 1,
        /// для которого A[j] меньше A[i]
        /// </summary>
        /// <param name="A"></param>
        /// <param name="D"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        private static (Int32, Boolean) FindMaxIndex(List<Int32> A, Int32[] D, Int32 i)
        {
            var max = 0;
            var found = false;
            
            for (var j = 0; j <= i - 1; j++)
            {
                if (A[j] < A[i])
                {
                    found = true;
                    
                    if (D[j] > D[max])
                    {
                        max = j;
                    }
                }
            }

            return (max, found);
        }
    }
}